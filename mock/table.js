import Mock from 'mockjs'

const data = Mock.mock({
  'items|30': [{
    id: '@id',
    title: '@sentence(10, 20)',
    'status|1': ['published', 'draft', 'deleted'],
    author: 'name',
    display_time: '@datetime',
    pageviews: '@integer(300, 5000)'
  }]
})

export default [
  {
    url: '/table/list',
    type: 'get',
    response: () => {
      const items = data.items
      return {
        status: 200,
        data: {
          total: items.length,
          items: items
        }
      }
    }
  },
  {
    url: '/table/test',
    type: 'get',
    response: () => {
      return {
        status: 200,
        data: {
          name: '张三',
          age: 24
        }
      }
    }
  }
]
