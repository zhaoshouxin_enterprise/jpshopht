import request from '@/utils/request'

//基本信息
/**
 * 小程序基本信息
 * @param {*} params
 */
export function getBaseInfo(params) {
  return request({
    url: '/miniConfig',
    method: 'get',
    params
  })
}
/**
 * 修改小程序基础配置
 * @param {*} params
 */
export function putBaseInfo(params) {
  return request({
    url: '/miniConfig',
    method: 'put',
    data: params
  })
}
//基本配置
/**
 * 小程序基础配置
 * @param {*} params
 */
export function getBaseConfig(params) {
  return request({
    url: '/merchantCon',
    method: 'get',
    params
  })
}
/**
 * 修改小程序基础配置
 * @param {*} params
 */
export function putBaseConfig(params) {
  return request({
    url: '/merchantConfig',
    method: 'put',
    data: params
  })
}
/**
 * 小程序授权地址
 * @param {*} params
 */
export function getAppletUrl(params) {
  return request({
    url: 'https://api.juanpao.com/wechat/officialAccount/openplat',
    method: 'get',
    params
  })
}

//上传发布

/**
 * 获取小程序发布信息
 * @param {*} params
 */
export function getAppletPublish(params) {
  return request({
    url: '/miniProgram',
    method: 'get',
    params
  })
}
/**
 * 获取体验二维码
 * @param {*} params
 */
export function getCode(params) {
  return request({
    url: 'https://api.juanpao.com/miniProgramQrcode',
    method: 'get',
    params
  })
}

//主题配色

/**
 * 获取主题配色数据
 * @param {*} params
 */
export function getAppletColor(params) {
  return request({
    url: '/merchantTheme',
    method: 'get',
    params
  })
}
/**
 * 保存主题配色数据
 * @param {*} params
 */
export function putAppletColor(params) {
  return request({
    url: '/merchantTheme',
    method: 'put',
    data: params
  })
}
