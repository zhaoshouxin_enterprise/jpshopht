import request from '@/utils/request'

//清理缓存

/**
 * 清理redis缓存
 * @param {*} params 
 */
export function cleanCache() {
  return request({
    url: '/shop/test/clear',
    method: 'get',
  })
}
