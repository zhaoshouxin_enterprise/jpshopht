
const goods= {
  // type: 1,
  // name: '',
  // short_name: '',
  // simple_info: '', //简单说明
  // code: '123', //商品货号
  // unit: '', //商品单位
  attribute: [''], //商品属性数组
  //m_category_id: '',//商品分组
  // city_group_id: '',//商品区域
  // label: '微信支付,',//增值服务
  // pic_urls: '', //商品主图
  video_id: '', //上传视频返回的参数
  video_pic_url: '' , //上传视频返回的参数
  video_url: '', //视频地址

  // detail_info: '', //商品的详细说明
  // is_open_assemble: '1', //是否开启拼团
  is_self: '1', //单独购买
  is_automatic: '0', //虚拟成团
  older_with_newer: '1', //老带新
  is_leader_discount: '1', //开团人优惠
  is_show: '1', //显示团长优惠
  tuan_type: '1',//拼团类型
  assemble_number: '',//成团人数
  assemble_group_discount: '', //团长优惠比例

  // stock_type: '1', //单规格双规格

  // price: '', //商品价格
  // line_price: '', //划线价
  // stocks: '', //库存
  // sales_number: '', //虚拟销量

  // is_limit: '1', //是否限量
  // limit_number: '', //限量个数

  // commission_selfleader_ratio: '', //团长佣金
  // commission_leader_ratio: '', //推客佣金？自提点佣金？

  // start_type: '0', //上架时间类型
  // start_time: '', //上架时间
  // end_time: '', //下架时间
  // take_goods_time: '', //提货时间

  // regimental_only: '0', //团长专属

  // sort: '', //排序

  // property1: '',
  // property2: '',

  // assemble_price: '', //拼团价

  // weight: 0,

  // status: '1',

  // have_stock_type: '1', //是否有规格

  type: 1,
  name: '',
  short_name: '',
  code: '',
  m_category_id: '',
  city_group_id: '',
  label: '微信支付,',
  pic_urls: '',
  simple_info: '',
  detail_info: '',
  property1: '',
  property2: '',
  assemble_price: '',
  price: '',
  weight: '',
  line_price: '',
  stocks: '',
  stock: [],
  sales_number: '',
  commission_leader_ratio: '',
  commission_selfleader_ratio: '',
  start_type: '0',
  start_time: '',
  end_time: '',
  take_goods_time: '',
  sort: '',
  status: '1',
  have_stock_type: '0',
  stock_type: '1',
  is_limit: '0',
  regimental_only: '',
  unit: '件',
  limit_number: '',
  is_open_assemble: '0'
}
const rules= {
  name: [
    { required: true, message: '请输入商品标题', trigger: 'blur' },
    { max: 60, message: '长度在60个字符内', trigger: 'blur' }
  ],
  short_name: [
    { max: 12, message: '短标题限12个字内', trigger: 'blur' }
  ],
  simple_info: [
    { max: 200, message: '简单说明限200个字内', trigger: 'blur' }
  ],
  unit: [
    { required: true, message: '请输入商品单位', trigger: 'blur' },
  ],
  price: [
    { required: true, message: '请输入商品价格', trigger: 'blur' },
  ],
  stocks: [
    { required: true, message: '请输入商品库存', trigger: 'blur' },
  ],
  pic_urls: [
    { required: true, message: '请选择商品主图', trigger: 'blur' },
  ],
  m_category_id: [
    { required: true, message: '请选择商品分组', trigger: 'blur' },
  ],
  detail_info: [
    { required: true, message: '请填写商品详情', trigger: 'blur' },
  ]
}
const cutPrice = { //砍价
  isOpen: '1', //开启砍价
  activeTime: [], //活动时间
  isSelf: '0', //单独购买
  virtualCount: '', //虚拟发起砍价数
  virtualPeopCount: '', //虚拟帮砍人数
  floorPrice: '', //最终底价
  friendCutCount: '', //好友帮砍次数
  cutTime: '', //砍价时间限制
  cutRules: [ {price: '', start: '', end: ''} ], //砍价规则
}

const property = [ //规格
  {
    name: '',
    value: []
  }
]

function upDateFormat(params) {
  return {
    id: params.id,
    type: params.type,
    name: params.name,
    short_name: params.short_name,
    code: params.code,
    m_category_id: params.m_category_id,
    city_group_id: params.city_group_id,
    label: params.label,
    pic_urls: params.pic_urls,
    simple_info: params.simple_info,
    detail_info: params.detail_info,
    property1: params.property1,
    property2: params.property2,
    assemble_price: params.assemble_price,
    price: params.price,
    weight: params.weight,
    line_price: params.line_price,
    stocks: params.stocks,
    stock: params.stock,
    sales_number: params.sales_number,
    commission_leader_ratio: params.commission_leader_ratio,
    commission_selfleader_ratio: params.commission_selfleader_ratio,
    start_type: params.start_type,
    start_time: params.start_time,
    end_time: params.end_time,
    take_goods_time: params.take_goods_time,
    sort: params.sort,
    status: params.status,
    have_stock_type: params.have_stock_type,
    stock_type: params.stock_type,
    key: params.key,
    video_id: params.video_id,
    video_pic_url: params.video_pic_url,
    video_url: params.video_url,
    is_limit: params.is_limit,
    regimental_only: params.regimental_only,
    unit: params.unit,
    limit_number: params.limit_number,
    is_open_assemble: params.is_open_assemble
  }
}


export { goods, rules, cutPrice, property, upDateFormat }

