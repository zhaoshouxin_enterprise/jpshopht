import defaultSettings from '@/settings'

const { showSettings, fixedHeader, sidebarLogo } = defaultSettings

const state = {
  showSettings: showSettings,
  fixedHeader: fixedHeader,
  sidebarLogo: sidebarLogo,
  routers:  [],
  menuIndex: 0,
  menuItemIndex: 0,
}

const mutations = {
  CHANGE_SETTING: (state, { key, value }) => {
    if (state.hasOwnProperty(key)) {
      state[key] = value
    }
  },
  Routers: (state,value) => {
    state.routers = value
  },
  MenuIndex: (state,value) => {
    state.menuIndex = value
  },
  MenuItemIndex: (state,value) => {
    state.menuItemIndex = value
  }
}

const actions = {
  changeSetting({ commit }, data) {
    commit('CHANGE_SETTING', data)
  },
  Routers({ commit }, data){
    commit('Routers', data)
  },
  MenuIndex({ commit }, data){
    commit('MenuIndex', data)
  },
  MenuItemIndex({ commit }, data){
    commit('MenuItemIndex',data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

