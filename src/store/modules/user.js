import { login } from '@/api/user'
import { getToken, setToken, removeToken, getName, setName, getPassWord, setPassWord, removeActiveApp } from '@/utils/auth'
import { loginRouter } from '@/router'

import loginTest from '@/testData/loginTest'

const state = {
  token: getToken(),
  name: getName(),
  password: getPassWord(),
  avatar: ''
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_PASSWORD: (state, password) => {
    state.password = password
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    const params = {
      name: username,
      password: password
    }
    return new Promise((resolve, reject) => {
      login(params).then(response => {
        const { data, name } = response
        commit('SET_TOKEN', data)
        commit('SET_NAME', name)
        setToken(data)
        setName(username)
        setPassWord(password)
        localStorage.setItem('juanpao', '{"access_token": "' + data + '"}')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  loginTest({ commit }, userInfo) {
    const { username, password } = userInfo;
    commit('SET_TOKEN', loginTest.token)
    commit('SET_NAME', '15088844694')
    setToken(loginTest.token)
    setName(username)
    setPassWord(password)
  },

  // get user info
  getInfo({ commit }) {
    return new Promise((resolve, reject) => {
      login({ username: getName(), password: getPassWord() }).then(response => {
        const { data, name } = response
        commit('SET_TOKEN', data)
        commit('SET_NAME', name)
        setToken(data)
        // setName(name)
        // setPassWord(password)
        resolve(data)
      }).catch(error => {
        reject(error)
      })

      // getInfo(state.token).then(response => {
      //   const { data } = response

      //   if (!data) {
      //     reject('Verification failed, please Login again.')
      //   }

      //   const { name, avatar } = data

      //   commit('SET_NAME', name)
      //   commit('SET_AVATAR', avatar)
      //   resolve(data)
      // }).catch(error => {
      //   reject(error)
      // })
    })
  },

  // user logout
  logout({ commit }) {
    // return new Promise((resolve, reject) => {
    //   logout(state.token).then(() => {
    //     commit('SET_TOKEN', '')
    //     removeToken()
    //     removeActiveApp()
    //     // resetRouter()
    //     loginRouter()
    //     resolve()
    //   }).catch(error => {
    //     reject(error)
    //   })
    // })

    commit('SET_TOKEN', '')
    removeToken()
    removeActiveApp()
    // resetRouter()
    loginRouter()
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      removeToken()
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

